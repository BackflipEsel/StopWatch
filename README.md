# Stop Watch
This is a simple stop watch utility, you can use to efficiently time tasks in your project.

## Download
[Latest] (https://mega.nz/#!rDY3TCCY!i2HYEjWyWSqMhlThUptMcDPO6XBQfqIF_3OdBeb0Uzc)

## Example
A basic example on how to use the stop watch:
```
public class SomeClass {
    private final StopWatch stopWatch = new StopWatch(); //Stop Watch instance.

    public void someMethod() {
        if (this.stopWatch.hasReached(2, TimeUnit.SECONDS)) { // Check if the time has reached 2 seconds.
            System.out.println(this.stopWatch.getElapsedTime(TimeUnit.SECONDS)); // Prints out the currently elapsed time in the given time unit.
            this.stopWatch.reset(); // Resets the stop watch.
        }
    }
}
```