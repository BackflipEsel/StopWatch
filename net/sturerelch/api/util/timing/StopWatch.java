package net.sturerelch.api.util.timing;

import java.util.concurrent.TimeUnit;

/**
 * The class {@code StopWatch} provides basic functions for efficient timing.
 *
 * @author BackflipEsel
 * */
public final class StopWatch {
    private long previousTime = System.nanoTime(); // The time when the stopwatch was created.

    /**
     * Returns if the stop watch has passed the given delay.
     *
     * @param delay the delay to be checked
     * @param timeUnit the time unit in which the delay should be checked
     * @return if the delay has been reached
     * */
    public boolean hasReached(final long delay, final TimeUnit timeUnit) {
        return timeUnit.convert(System.nanoTime() - this.previousTime, TimeUnit.NANOSECONDS) >= delay;
    }

    /**
     * Resets the previous time.
     * */
    public void reset() {
        this.previousTime = System.nanoTime();
    }

    /**
     * Returns the elapsed time since the last reset or creation of the stop watch.
     *
     * @param timeUnit the time unit you want the elapsed time to be returned in
     * @return the elapsed time
     * */
    public long getElapsedTime(final TimeUnit timeUnit) {
        return timeUnit.convert(System.nanoTime() - this.previousTime, TimeUnit.NANOSECONDS);
    }
}
